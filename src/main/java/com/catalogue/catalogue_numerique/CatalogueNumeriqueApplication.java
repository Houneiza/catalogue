package com.catalogue.catalogue_numerique;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class CatalogueNumeriqueApplication {

	public static void main(String[] args) {
		SpringApplication.run(CatalogueNumeriqueApplication.class, args);
	}

}
